<?php
$page = $_REQUEST['page'];
$home = 'home';
$entry = 'entry';

if (!$page){
	require_once './template/home.php';
}

if ($page === 'home'){
	require_once './template/home.php';
}

if ($page === 'entry'){
	require_once './template/entry.php';
}
if ($page === 'edit'){
	require_once './template/edit.php';
}

if ($page === 'dm'){
	require_once './template/dm.php';
}

$data = $_REQUEST['data'];
if($data === 'new_entry'){
	$name = $_REQUEST['name'];
	$date = $_REQUEST['date'];
	$type = $_REQUEST['type'];
	$src = json_decode(file_get_contents('./data/roster.json'),'true');
	$tempArray = $src;
	$tempArray[$date]['players'][$name] = $type;
	$entry = $tempArray;
	file_put_contents('./data/roster.json',json_encode($entry));

}

if($data === 'del_entry'){
	$name = $_REQUEST['name'];
	$date = $_REQUEST['date'];
	$type = $_REQUEST['type'];
	$src = json_decode(file_get_contents('./data/roster.json'),'true');
	$tempArray = $src;
	$tempArray[$date]['players'][$name] = 'CANCELLED';
	$entry = $tempArray;
	file_put_contents('./data/roster.json',json_encode($entry));

}

if($data === 'dm_entry'){
	$dmName = $_REQUEST['dmName'];
	$date = $_REQUEST['date'];
	$theme = $_REQUEST['theme'];
	$restrictions = $_REQUEST['restrictions'];
	$description = $_REQUEST['description'];
	$key = $_REQUEST['dmKey'];


	$src = json_decode(file_get_contents('./data/roster.json'),'true');
	$tempArray = $src;
	$tempArray[$date]['dm'] = $dmName;
	$tempArray[$date]['theme'] = $theme;
	$tempArray[$date]['restrictions'] = $restrictions;
	$tempArray[$date]['description'] = $description;
	$tempArray[$date]['dmKey'] = $key;

	$entry = $tempArray;
	file_put_contents('./data/roster.json',json_encode($entry));

}
if($data === 'dm_edit'){
	$date = $_REQUEST['date'];
	$theme = $_REQUEST['theme'];
	$restrictions = $_REQUEST['restrictions'];
	$description = $_REQUEST['description'];



	$src = json_decode(file_get_contents('./data/roster.json'),'true');
	$tempArray = $src;
	$tempArray[$date]['theme'] = $theme;
	$tempArray[$date]['restrictions'] = $restrictions;
	$tempArray[$date]['description'] = $description;


	$entry = $tempArray;
	file_put_contents('./data/roster.json',json_encode($entry));

}


