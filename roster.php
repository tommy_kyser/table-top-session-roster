<?php
$roster = json_decode( file_get_contents( './data/roster.json' ), 'true' );
?>
<div class="container-fluid roster">
<?php foreach ( $roster as $friday => $key ) { ?>
		<div class='row session'>
			<div class="col-12 text-center day">
				<h2 class="date"><?php echo date("l M jS", strtotime($friday)); ?> <a href="?page=edit&session=<?php echo $friday; ?>"><i class="far fa-edit"></i></a></h2>
			</div>
			<div class="col-12 text-center meta">
				<h4><?php echo $key['theme']; ?></h4>
				<h5><?php echo $key['restrictions']; ?></h5>
				<p><?php echo $key['description']; ?></p>

			</div>
			<div class="col-12 player-list text-center">
				<span class="dm"><b><?php echo $key['dm']; ?></b></span>
				<span class="type text-right"><em>DM</em></span>
			</div>
			<?php
			foreach ( $key['players'] as $player =>$class ) { ?>
				<div class="col-12 player-list text-center">
					<span class="player"><b><?php echo $player; ?></b></span>
					<span class="type text-right"><em><?php echo $class; ?></em></span>
				</div>
				<?php
			}
			?>
		</div>
	<div class="container-fluid roster-controls">
		<div class='row'>
			<div class="col-12 text-center">
				<a role="button" class=" add-btn btn btn-primary" href="?page=entry&session=<?php echo $friday; ?>">Join This Session</a>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<div class="container-fluid roster-controls">
	<div class='row'>
		<div class="col-12 text-center">
			<a role="button" class="session-btn btn btn-secondary" href="?page=dm">Create A Session</a>
		</div>
	</div>
</div>
<script src="/js/roster.js"></script>