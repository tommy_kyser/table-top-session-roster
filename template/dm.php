<div class="container-fluid">
	<div class='row top-info'>
		<div class="col-12 text-center">
			<h3>Create A Session</h3>
			<p>
				Enter your session info and start prepping to run the show! Be sure and remember your DM password if you want to be able to go back and make changes.
			</p>
			<a role="button" class="home-btn btn btn-secondary" href="?page=home">Go Back</a>
		</div>
	</div>
</div>



<div class="container-fluid entry-wrap">
	<div class="row">
		<div class="col-12 text-center dm-col-top">
			<h4>DM A Session</h4>
		</div>
	</div>
	<div class='form-row'>
		<div class="col-12 text-center dm-col">
			<label for="dm-name">DM Name</label>
			<input id="dm-name" type="text" class="form-control" placeholder="Mantis Drexum">
		</div>
		<div class="col-12 text-center dm-col">
			<label  for="cal">Pick A Friday</label>
			<input id="cal" class="form-control flatpickr flatpickr-input form-control input active" placeholder="Select Date.." tabindex="0" type="text" readonly="readonly">
		</div>
		<div class="col-12 text-center dm-col">
			<label for="session-theme">Session Theme/Type</label>
			<input id="session-theme" type="text" class="form-control" placeholder="Forgotten Realms 5e">
		</div>
		<div class="col-12 text-center dm-col">
			<label for="session-restrictions">Character Restrictions</label>
			<input id="session-restrictions" type="text" class="form-control" placeholder="3rd Level 5000g">
		</div>
		<div class="col-12 text-center dm-col">
			<label for="session-details">Session Description</label>
			<textarea id="session-details" name="session-details" class="form-control" rows="10" cols="30" placeholder="This session will be about 100 random wands that each have a sexual power..."></textarea>
		</div>
		<div class="col-12 text-center dm-col">
			<label for="dm-key">Enter a DM Password (This Allows You To Manage Your Sessions)</label>
			<input id="dm-key" type="password" class="form-control" placeholder="Password">
		</div>
		<div class="col-12 text-center dm-col">
			<a role="button" class="save-btn btn btn-primary" href="#">Save</a>
			<a role="button" class="home-btn btn btn-secondary" href="?page=home" style="display: none;">Go To Roster</a>
		</div>
	</div>
</div>
<script src="/js/dm.js"></script>