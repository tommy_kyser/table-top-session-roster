<?php
$sessionDate = $_REQUEST['session'];
?>
<div class="container-fluid">
	<div class='row top-info'>
		<div class="col-12 text-center">
			<h3>Join A Session</h3>
			<p>
				Add your name to this session! If you're not sure you can make it, don't sweat it, just hold off on adding your name.
			</p>
			<a role="button" class="home-btn btn btn-secondary" href="?page=home">Go Back</a>
		</div>
	</div>
</div>


	<div class="container-fluid entry-wrap">
		<div class='form-row'>
			<div class="col-4">
				<label for="name">Name</label>
				<input id="name" type="text" class="form-control" placeholder="Tyreal Falreath">
			</div>
			<div class="col-4">
				<label for="date">Date</label>
				<input id="date" class="form-control" placeholder="<?php echo $sessionDate; ?>" type="text" readonly="readonly" value="<?php echo $sessionDate; ?>">
			</div>
			<div class="col-4">
				<label for="type">Joining as</label>
				<input id="type" type="text" class="form-control" placeholder="Character Name, Character Class">
			</div>
			<div class="col-12">
				<a role="button" class="save-btn btn btn-primary" href="#">Save</a>
				<a role="button" class="home-btn btn btn-secondary" href="?page=home" style="display: none;">Go To Roster</a>
			</div>
		</div>
	</div>
	<script src="/js/entry.js"></script>