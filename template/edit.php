<?php
$roster = json_decode( file_get_contents( './data/roster.json' ), 'true' );
$session  = $_REQUEST['session'];
$key    = $roster[$session];
$pass   = $key['dmKey'];
?>

<div class="container-fluid roster">
	<div class='row login'>
		<div class="col-12 text-center">
			<label for="dm-pass">Enter The DM Password For This Session</label>
			<input id="dm-pass" type="password" class="form-control" placeholder="Password">
			<button id="enter-pass" class="btn btn-primary" type="submit">Enter</button>
		</div>

		<script>
			$(function () {
				var pass = '<?php echo $pass; ?>' ;
				var enterBtn = $('#enter-pass');
				enterBtn.click(function () {
					if($('#dm-pass').val() === pass){
						$('.session').removeClass('locked');
						enterBtn.removeClass('btn-primary');
						enterBtn.addClass('btn-success');
						enterBtn.text('Success!');
					}
				});
			});
		</script>

	</div>
	<div class='row session locked'>
		<div class="col-12 text-center day">
			<h2 class="date"><?php echo date( "l M jS", strtotime( $date ) ); ?></h2>
		</div>
		<div class="col-12 text-center meta">
			<div class="col-12 text-center dm-col">
				<label for="session-theme">Edit Session Theme/Type</label>
				<input id="session-theme" type="text" class="form-control" value="<?php echo $key['theme']; ?>">
			</div>
			<div class="col-12 text-center dm-col">
				<label for="session-restrictions">Character Restrictions</label>
				<input id="session-restrictions" type="text" class="form-control" value="<?php echo $key['restrictions']; ?>">
			</div>
			<div class="col-12 text-center dm-col" style="margin-bottom: 50px;">
				<label for="session-details">Session Description</label>
				<textarea id="session-details" name="session-details" class="form-control" rows="10" cols="30" ><?php echo $key['description']; ?></textarea>
				<a role="button" id="edit-meta" class="btn btn-primary" href="#" style="margin-top: 50px;">Save</a>
			</div>

		</div>
		<div class="col-12 player-list text-center">
			<span class="dm"><b><?php echo $key['dm']; ?></b></span>
			<span class="type text-right"><em>DM</em></span>
		</div>
		<?php
		foreach ( $key['players'] as $player => $class ) { ?>
			<div class="col-12 player-list text-center">
				<span class="player"><b><a class="" href="?page=edit"><?php echo $player; ?></a></b></span>
				<span class="type text-right"><em><?php echo $class; ?></em></span>
				<a role="button" class="del-btn btn btn-warning" data-date="<?php echo $friday; ?>" data-name="<?php echo $player; ?>" data-type="<?php echo $class; ?>">CANCEL</a>
			</div>
			<?php
		}
		?>
	</div>
	<div class="col-12  text-center">
		<a role="button" class="home-btn btn btn-secondary text-center" href="?page=home" style="display: initial;">Go To Roster</a>
	</div>
</div>
<script src="/js/edit.js"></script>

<script>
	var sessionDate = '<?php echo $session;?>';
	console.log(sessionDate);
	var editMeta = $('#edit-meta');
	editMeta.click(function () {
		var date = sessionDate;
		var theme = $('#session-theme').val();
		var restrictions = $('#session-restrictions').val();
		var details = $('#session-details').val();
		console.log(date + theme + restrictions + details);
		$.post("?data=dm_edit&date=" +date + "&theme=" + theme + "&restrictions=" + restrictions + "&description=" + details,
			function (data, status) {

			});
		editMeta.text('Saved!');
		editMeta.removeClass('btn-primary');
		editMeta.addClass('btn-success disabled');

	});
</script>